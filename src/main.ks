// runpath("0:/K-FOLD/src/main.ks").

@lazyglobal off.
global is_silent is false.
global launch_profile is "retrograde_10_lz3".

// @name define_globals:
//   Export flight profile information.

function define_globals {

  // Static configuration
  global lz_height is 23.5.
  global boostback_pitch is 5.
  global throttle_minimum is 0.50.
  global legs_deploy_altitude is 1000.
  global launch_clear_altitude is 150.
  global throttle_predicted_max is 0.95.
  global boostback_lock_distance is 2500.
  global booster_separation_speed is 1000.
  global preboostback_maxstoppingtime is 1.
  global postboostback_maxstoppingtime to 1.

  // All possible targets
  global jrti is "Just Read The Instructions".
  global ocisly is "Of Course I Still Love You".
  global lz1 is "LZ-1". global lz2 is "LZ-2".
  global lz3 is "LZ-3". global lz4 is "LZ-4".
}

// @name setup:
//   Perform basic global setup.

function setup {

  parameter _left_target.
  parameter _right_target.

  // Vehicle component detection
  global is_left_booster is (core:tag = "left").
  global is_right_booster is (core:tag = "right").
  global is_core_booster is not (is_left_booster or is_right_booster).

  // Compute target
  local target_name is _right_target.
  
  if is_left_booster {
    set target_name to _left_target.
  }

  // Globals: target information
  lock lz to vessel(target_name).
  lock lz_geo to lz:geoposition.
  global lz_altitude is lz:altitude + lz_height.

  // Globals: location/GIS information
  lock alt to ship:altitude.
  lock ship_geo to ship:geoposition.
  lock impact_geo to addons:tr:impactpos.
  lock hdg_deg to mod(360 - latlng(90, 0):bearing, 360).

  // Globals: velocity information
  lock v_current to ship:airspeed.
  lock vg_current to ship:groundspeed.
  lock vy_current to ship:verticalspeed.

  // Global: pitch information
  lock pitch_deg to (
    90.0 - vectorangle(ship:up:vector, ship:facing:forevector)
  ).
  
  // Global: prograde pitch information
  lock prograde_pitch_deg to (
    90.0 - vectorangle(ship:up:vector, ship:srfprograde:vector)
  ).
  
  // Global: retrograde pitch information
  lock retrograde_pitch_deg to (
    90.0 - vectorangle(ship:up:vector, ship:srfretrograde:vector)
  ).

  // Available downrange thrust
  lock downrange_thrust to (
    ((ship:availablethrust * throttle_predicted_max) / ship:mass)
      * sin(90 - pitch_deg)
  ).

  // Downrange distance to target
  lock x to (lz_geo:position - ship_geo:position):mag.

  // Downrange distance needed to reach velocity goal
  lock x_needed to (
    (vg_current^2 - final_velocity_goal^2) / (2 * downrange_thrust)
  ).

  // Downrange distance until burn deadline
  lock x_delta to x - x_needed - final_distance_goal.

  // Time until entry burn deadline
  lock entry_deadline to (x_delta / vg_current).
}

// @name find_angular_error:
//   Compute a relative approach angle error between two vessels.

function find_angular_error {

  parameter _lz_geo.
  parameter _ship_geo.
  parameter _impact_geo.

  local v_impact is V(
    _impact_geo:lng - _lz_geo:lng, _impact_geo:lat - _lz_geo:lat, 0
  ).

  local v_ship is V(
    _ship_geo:lng - _lz_geo:lng, _ship_geo:lat - _lz_geo:lat, 0
  ).

  local angular_error is vang(v_ship, v_impact).
  local is_negative is (vcrs(v_ship, v_impact):z < 0).

  if (angular_error > 90) {
    set angular_error to 180 - angular_error.
  }

  if (is_negative) {
    set angular_error to -angular_error.
  }

  return angular_error.
}

// @name deflect_from_retrograde:
//   Deflect from retrograde in the coplanar x and y directions.

function deflect_from_retrograde {

  parameter _x.
  parameter _y.

  local v is (
    angleaxis(_y, ship:srfretrograde:starvector)
      * ship:srfretrograde
  ).

  return lookdirup(
    (angleaxis(_x, v:topvector) * v):vector,
      ship:facing:topvector
  ).
}

// @name statistics:
//   Print a whole lot of useful telemetry information.

function statistics {

  local c is constant:AtmToKPa.

  emit("").
  emit("Pitch: " + round(pitch_deg, 2) + "°").
  emit("Heading: " + round(hdg_deg, 2) + "°").
  emit("Air speed: " + round(v_current, 2) + "m/s").
  emit("Ground speed: " + round(vg_current, 2) + "m/s").
  emit("Prograde pitch: " + round(prograde_pitch_deg, 2) + "°").
  emit("Vertical speed: " + round(vy_current, 2) + "m/s").
  emit("Vehicle dynamic pressure: " + round(c * ship:q, 2) + "kPa").
}

// @name launch:
//   Automated launch/ascent program.

function launch {

  // Steering manager setup
  steeringmanager:resettodefault().
  set steeringmanager:yawts to 40.
  set steeringmanager:rollts to 6.
  set steeringmanager:pitchts to 40.

  // Toward space
  local initial_up to lookdirup(
    ship:up:vector, ship:facing:topvector
  ).

  // Countdown
  local t is 10.
  sas off. rcs off. unlock steering.

  // Self-align
  until (t <= 5) {
    titled().
    emit("Launch: T-" + round(t, 2) + ".00s").
    emit("Vehicle is in self-align").
    statistics().
    set t to t - 1. wait 1.
  }

  // Configured for launch
  lock throttle to 1.
  lock steering to initial_up.

  // Terminal count
  until (t <= 0) {
    titled().
    emit("Launch: T-" + round(t, 2) + ".00s").
    emit("Terminal count: vehicle is configured for flight").
    statistics().
    set t to t - 1. wait 1.
  }

  // Launch
  stage.

  // Wait for altitude
  until (launch_clear_altitude <= alt) {
    titled().
    emit("Vehicle liftoff").
    statistics().
  }

  // Roll program
  lock steering to initial_up + R(0, 0, launch_roll_degrees).

  // Wait for pitchover velocity
  until (v_current >= launch_pitchover_velocity) {
    titled().
    emit("Vehicle has cleared the tower").
    statistics().
  }

  // Steering manager setup
  set steeringmanager:yawts to 10.
  set steeringmanager:pitchts to 10.
  set steeringmanager:maxstoppingtime to 1.

  // Pitchover manuver
  lock steering to heading(
    90 + launch_roll_degrees, 90 - launch_pitchover_degrees
  ).

  // Wait for pitchover
  lock pitch_complete to (
    abs(pitch_deg - (90 - launch_pitchover_degrees)) <= 5
  ).

  // Wait for pitchover completion
  until (pitch_complete) {
    titled().
    emit("Vehicle is pitching downrange").
    statistics().
  }

  // Prograde lock-on
  lock prograde_lock to (
    abs(pitch_deg - prograde_pitch_deg) <= 1
  ).

  // Wait for prograde lock
  until (prograde_lock) {
    titled().
    emit("Vehicle pitchover complete").
    statistics().
  }

  // Lock to prograde
  lock steering to lookdirup(
    velocity:surface, ship:facing:topvector
  ).
}

// @name fly_vehicle_ascent:
//   Fly the vehicle to to a velocity goal, then stage.

function fly_vehicle_ascent {

  parameter _velocity_goal.
  parameter _launch_failsafe.

  phase("Core Vehicle Ascent").

  if (_launch_failsafe) {
    launch().
  }

  // Wait for separation velocity
  until (v_current >= _velocity_goal) {
    titled().
    emit("Vehicle is on a nominal trajectory").
    statistics().
  }

  // Side booster startup
  processor("left"):connection:sendmessage("boot").
  processor("right"):connection:sendmessage("boot").

  // Side booster separation
  rcs on. ag6 on. rcs off.

  titled().
  emit("Booster separation commanded").
  statistics().
}

// @name wait_for_separation:
//   Wait for separation command from core booster.

function wait_for_separation {

  if is_left_booster {
    phase("Left Booster Ascent").
  } else if is_right_booster {
    phase("Right Booster Ascent").
  }

  until not core:messages:empty {
    titled().
    emit("Waiting for separation command").
    statistics().
  }

  titled().
  emit("Separation confirmed").
}

// @name pre_boostback_flip:
//   Flip side booster around for boostback burn.

function pre_boostback_flip {

  phase("Stage One Fast Flip").

  // Steering manager setup
  steeringmanager:resettodefault().
  set steeringmanager:yawts to 10.
  set steeringmanager:pitchts to 10.
  set steeringmanager:maxstoppingtime to preboostback_maxstoppingtime.

  titled().
  emit("Booster status: separating").

  // Engine setup 
  lock throttle to 0.
  ag3 on. rcs on. sas off.

  // Move away from core booster
  set ship:control:starboard to -1.0.
  wait 7.5.
  set ship:control:starboard to 0.

  // Flip
  lock steering to lookdirup(
    heading(lz:heading, boostback_pitch):vector,
      ship:facing:topvector
  ).

  lock flip_complete to (
    abs(hdg_deg - lz:heading) <= 10
      and abs(pitch_deg - boostback_pitch) <= 45
  ).

  until flip_complete {
    titled().
    emit("Booster status: in flip").
    emit("Commanded pointing: " + round(lz:heading, 2) + "°").
    statistics().
  }
}

// @name boostback:
//   Nullify horizontal velocity, targeting the landing zone.

function boostback {
 
  // Steering manager setup
  steeringmanager:resettodefault().

  if is_left_booster {
    phase("S1 Boostback Burn: Left").
  } else if is_right_booster {
    phase("S1 Boostback Burn: Right").
  }

  // Impact prediction requires active vessel
  if is_right_booster {
    emit("Status: No impact prediction available").
    wait_forever().
  }

  // Impact prediction requires target vessel
  if not is_core_booster {
    kuniverse:forcesetactivevessel(ship).
  }

  // Downrange distance calculation
  lock distance_to_lz to lz_geo:position:mag.
  lock distance_to_impact to impact_geo:position:mag.
  lock distance_error to (lz_geo:position - impact_geo:position):mag.

  // PID controller for angular error
  local pa is pidloop(1.25, 0, 0, -35, 35).
  set pa:setpoint to 0.

  // Light this candle
  lock throttle to 1.

  local goal_reached is false.
  local in_overshoot is false.

  // Until we reach our impact prediction goal...
  until goal_reached {

    titled().

    if (distance_error < boostback_lock_distance) {
      set in_overshoot to true.
    }
    
    if (in_overshoot) {
      set goal_reached to (
        (distance_to_impact - distance_to_lz) > boostback_overshoot
      ).
    }

    // Calculate error
    local angular_error is (
      find_angular_error(lz_geo, ship_geo, impact_geo)
    ).

    // Compute steering offset
    local offset_deg is pa:update(time:seconds, angular_error).

    // Correct steering direction
    local commanded_pointing is lz:heading + offset_deg.

    lock steering to lookdirup(
      heading(commanded_pointing, boostback_pitch):vector,
        ship:facing:topvector
    ).

    emit("Angular error: " + round(angular_error, 6) + "°").
    emit("Steering offset: " + round(offset_deg, 2) + "°").
    emit("Landing zone heading: " + round(lz:heading, 2) + "°").
    emit("Distance error: " + round(distance_error, 2) + "m").
    emit("Distance to landing: " + round(distance_to_lz, 2) + "m").
    emit("Distance until impact: " + round(distance_to_impact, 2) + "m").
    emit("Commanded pointing: " + round(commanded_pointing, 2) + "°").
    emit("Prograde pitch: " + round(prograde_pitch_deg, 2) + "°").
    statistics().
  }

  // Boostback cutoff
  lock throttle to 0.
  unlock steering.
}

// @name post_boostback_flip:
//   Flip back around to reorient for the coast phase.

function post_boostback_flip {
 
  phase("S1 Post-Boostback Flip").
  rcs on.

  // Steering manager setup
  steeringmanager:resettodefault().
  set steeringmanager:yawts to 10.
  set steeringmanager:pitchts to 10.
  set steeringmanager:maxstoppingtime to postboostback_maxstoppingtime.

  // Don't cross below horizon
  lock flip_pitch to max(0, retrograde_pitch_deg).

  // Reverse course
  lock aim_deg to mod(lz:heading + 180, 360).

  // Perform flip manuver
  lock steering to lookdirup(
    heading(aim_deg, flip_pitch):vector, ship:facing:topvector
  ).

  // Wait for flip
  lock flip_complete to (
    abs(hdg_deg - aim_deg) <= 5
      and abs(pitch_deg - flip_pitch) <= 10
  ).

  until (flip_complete) {
    titled().
    emit("Status: in post-boostback flip").
    emit("Retrograde pitch: " + round(retrograde_pitch_deg, 2) + "°").
    emit("Heading goal: " + round(aim_deg, 2) + "°").
    emit("Pitch goal: " + round(flip_pitch, 2) + "°").
    emit("Heading error: " + round(abs(hdg_deg - aim_deg), 2) + "°").
    emit("Pitch error: " + round(abs(pitch_deg - flip_pitch), 2) + "°").
    statistics().
  }

  local last_alt is alt.
  lock is_descending to (alt < last_alt).

  until is_descending {

    titled().
    emit("Status: waiting for apogee").
 
    // Informational only
    local angular_error is (
      find_angular_error(lz_geo, ship_geo, impact_geo)
    ).

    emit("Angular error: " + round(angular_error, 6) + "°").
    statistics().

    set last_alt to alt.
    wait 0.25.
  }
}

// @name atmospheric_coast:
//   Coast downrange, eventually locking to retrograde.

function atmospheric_coast {

  phase("Atmospheric coast").
  rcs off.

  when (entry_deadline <= 5) then {
    rcs on.
  }

  // Steering manager setup
  steeringmanager:resettodefault().
  set steeringmanager:yawts to 10.
  set steeringmanager:pitchts to 10.
  set steeringmanager:maxstoppingtime to 6.

  // Gridfin deploy
  ag9 on.

  // Lock to retrograde
  lock steering to lookdirup(
    -velocity:surface, ship:facing:topvector
  ).

  // Wait for entry burn deadline
  until (x_delta <= 0) {

    // Informational only
    local angular_error is (
      find_angular_error(lz_geo, ship_geo, impact_geo)
    ).

    titled().
    emit("Entry burn: T-" + round(entry_deadline, 2) + "s").
    emit("Angular error: " + round(angular_error, 6) + "°").
    emit("Downrange thrust: " + round(downrange_thrust, 2) + "m/s²").
    emit("Distance until entry burn: " + round(x_delta, 2) + "m").
    emit("Downrange distance remaining: " + round(x, 2) + "m").
    emit("Distance required for entry burn: " + round(x_needed, 2) + "m").
    statistics().
  }
}

// @name entry_burn:
//   Reduce entry velocity while correcting toward the landing zone.

function entry_burn {

  phase("Entry startup").
  rcs on.

  // Steering manager setup
  steeringmanager:resettodefault().

  // PID controller for downrange impact point error
  local pt is pidloop(0.015, 0.025, 0, throttle_minimum, 1).
  set pt:setpoint to 0.

  // PID controller for angular error correction
  local pa is pidloop(1.25, 0.1, 0, -10, 10).
  set pa:setpoint to 0.

  // Downrange distance to target
  lock distance_to_lz to (lz_geo:position - ship_geo:position):mag.

  // Until we reach our velocity goal...
  until (vg_current <= final_velocity_goal) {

    // Angular error correction
    local angular_error is (
      find_angular_error(lz_geo, ship_geo, impact_geo)
    ).

    local offset_deg is pa:update(time:seconds, angular_error).
    set steering to deflect_from_retrograde(-offset_deg, 0).

    // Throttle to account for decreasing pitch
    local t is pt:update(time:seconds, x_delta).
    lock throttle to t.

    titled().
    emit("Offset: " + round(offset_deg, 2) + "°").
    emit("Angular error: " + round(angular_error, 2) + "°").
    emit("Downrange distance error: " + round(x_delta, 2) + "m").
    statistics().
  }

  unlock steering.
  lock throttle to 0.
}

// @name terminal_guidance:
//   Perform gridfin-based lifting body steering, landing burn.

function terminal_guidance {

  phase("Terminal guidance").
  rcs on.

  // Steering manager setup
  steeringmanager:resettodefault().
  set steeringmanager:yawts to 10.
  set steeringmanager:pitchts to 10.

  // Instantaneous acceleration due to gravity
  lock g to body:mu / ((alt + body:radius) ^ 2).

  // Speed of sound at altitude
  lock mach to sqrt(2 / 1.4 * ship:q / body:atm:altitudepressure(alt)).

  // Available vertical thrust
  lock vertical_thrust to (
    ((ship:availablethrust * throttle_predicted_max) / ship:mass) - g
  ).

  // Vertical distance to target
  lock y to alt - (lz_altitude + 1).

  // Vertical distance needed to reach zero velocity
  lock y_needed to (
    (vy_current ^ 2 - 0) / (2 * vertical_thrust)
  ). 

  // Vertical distance until burn deadline
  lock y_delta to (y - y_needed).

  // PID controller for per-axis impact point error
  local px is pidloop(2.5, 0.35, 0.65, -45, 45).
  local py is pidloop(2.5, 0.35, 0.65, -45, 45).

  // Axis error goals
  set px:setpoint to 0.
  set py:setpoint to 0.

  // Guidance state
  local dx is 0. local dy is 0.
  local sx is 0. local sy is 0.

  // Landing phases
  local landing_commit is false.
  local landing_touchdown_in_progress is false.

  // PID controller for suicide burn throttle
  local pt is pidloop(0.015, 0.02, 0, throttle_minimum, 1).

  // Shutdown velocity goal
  set pt:setpoint to 0.

  // Asynchronous: landing commit
  when (y_delta <= 0) then {
    set landing_commit to true.
    set px:minoutput to -10. set px:maxoutput to 10.
    set py:minoutput to -10. set py:maxoutput to 10.
  }

  // Asynchronous: landing leg deployment
  when (alt <= legs_deploy_altitude) then {
    gear on. row(3).
  }

  // Get your towels ready
  until (vy_current >= 0) {

    titled().

    // Impact error calculation
    if (addons:tr:hasimpact) {
      set dx to (impact_geo:lat - lz_geo:lat) * 1000.
      set dy to (impact_geo:lng - lz_geo:lng) * 1000.
    } else {
      set dx to 0. set dy to 0.
    }

    // Vehicle steering
    set sy to py:update(time:seconds, dy).
    set sx to px:update(time:seconds, dx).

    // Landing burn startup
    if (landing_commit) {
      local t is pt:update(time:seconds, y_delta).
      rcs on. lock throttle to t.
    }

    // Thrust vectoring forces dominate
    if (landing_commit) {
      set sx to -sx. set sy to -sy.
    }

    set steering to lookdirup(
      -velocity:surface + R(sx, sy, 0), ship:facing:topvector
    ).

    if (landing_commit) {
      emit("Throttle: " + round(throttle * 100, 2) + "%").
    } else if (addons:tr:hasimpact) {
      emit("Landing burn: T-" + round(y_delta / -vy_current, 2) + "s").
    }

    emit("Guidance error: " + round(dx, 2) + ", " + round(dy, 2)).
    emit("Steering offset: " + round(sx, 2) + "°, " + round(sy, 2) + "°").
    emit("Vertical thrust: " + round(vertical_thrust, 2) + "m/s²").
    emit("Vertical distance error: " + round(y_delta, 2) + "m").
    emit("Vertical distance remaining: " + round(y, 2) + "m").

    statistics().
  }

  phase("Vehicle shutdown").

  titled().
  lock throttle to 0.
  emit("Status: Booster engine cutoff").
  wait 2. unlock steering. rcs off. ag9 off.
}

// @name celebrate:
//   Chant "USA USA USA" a give someone a jumping hug.

function celebrate {

  phase("Eyas 7 has landed").
  titled().

  emit("Of course I still love you."). wait 1.
  emit("Landing operators proceed with initial securing."). wait 1.
  emit("Move to procedure 11.100, section 3 on recovery net."). wait 15.

  clearscreen.
}

// @name wait_forever:
//   Wait forever and never return.

function wait_forever {
  until false {
    wait 3600.
  }
}

// @set row:
//   Set the cursor position to a particular row.

function row {
  parameter _r.
  set r to _r.
}

// @name clear:
//   Clear the console.

function clear {
  row(0).
  clearscreen.
}

// @name emit:
//   Write a full row of text to the console.

function emit {
  parameter _str.
  if (is_silent) { return. }
  print _str:padright(terminal:width) at (0, r).
  set r to r + 1.
}

// @name phase:
//   Report a new phase of the mission to the console.

function phase {
  parameter _name.
  clear(). print _name at (0, 0).
  print "":padright(_name:length):replace(" ", "=") at (0, 1).
  titled().
}

// @name titled:
//   Skip rows to accommodate for use of `phase`.

function titled {
  row(3).
}

// @name main:
//   Entry point. X gon deliver to ya.

function main {

  parameter _left_target.
  parameter _right_target.
 
  setup(_left_target, _right_target).

  if is_core_booster {
    fly_vehicle_ascent(booster_separation_speed, true).
    wait_forever().
  } else {
    wait_for_separation().
  }

  pre_boostback_flip().
  boostback().
  post_boostback_flip().

  atmospheric_coast().
  entry_burn().

  terminal_guidance().
  celebrate().
}

// And...
global r is 0.
clear().

// elon-blunt.gif
define_globals().
runpath("0:/K-FOLD/profiles/" + launch_profile + ".ks").

// T-Pain
main(left_target, right_target).

