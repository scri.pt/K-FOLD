// Polar 8: North (Droneship)

function static_gnc {
  global launch_roll_degrees is -50.
  global launch_pitchover_degrees is 9.
  global boostback_overshoot is 1000.
  global final_velocity_goal is 80.
  global final_distance_goal is 1250.
  global launch_pitchover_velocity is 100.
  global left_target is ocisly.
  global right_target is jrti.
}

static_gnc().
