// Polar 14: South (RTLS)

function static_gnc {
  global launch_roll_degrees is 60.
  global launch_pitchover_degrees is 14.
  global launch_pitchover_velocity is 110.
  global boostback_overshoot is 1000.
  global final_velocity_goal is 60.
  global final_distance_goal is 1000.
  global left_target is lz2.
  global right_target is lz3.
}

static_gnc().
