// Polar 14: South (Droneship)

function static_gnc {
  global launch_roll_degrees is 40.
  global launch_pitchover_degrees is 14.
  global launch_pitchover_velocity is 110.
  global boostback_overshoot is 1000.
  global final_velocity_goal is 60.
  global final_distance_goal is 1750.
  global left_target is ocisly.
  global right_target is jrti.
}

static_gnc().
