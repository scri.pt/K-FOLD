// Equatorial 15: 90º (Droneship)

function static_gnc {
  global boostback_pitch is 5.
  global launch_roll_degrees is 5.
  global launch_pitchover_degrees is 15.
  global launch_pitchover_velocity is 90.
  global boostback_overshoot is 1000.
  global final_velocity_goal is 80.
  global final_distance_goal is 2000.
  global left_target is ocisly.
  global right_target is ocisly.
}

static_gnc().
