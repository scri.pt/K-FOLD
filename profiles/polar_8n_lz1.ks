// Polar 8: North (RTLS)

function static_gnc {
  global launch_roll_degrees is -70.
  global launch_pitchover_degrees is 8.
  global launch_pitchover_velocity is 100.
  global boostback_overshoot is 1000.
  global final_velocity_goal is 60.
  global final_distance_goal is 3250.
  global left_target is lz1.
  global right_target is lz2.
}

static_gnc().
