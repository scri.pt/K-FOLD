// Retrograde 15: 270º (Droneship)

function static_gnc {
  global boostback_pitch is 7.5.
  global launch_roll_degrees is 180.
  global launch_pitchover_degrees is 15.
  global launch_pitchover_velocity is 100.
  global boostback_overshoot is 1000.
  global final_velocity_goal is 80.
  global final_distance_goal is 2000.
  global left_target is ocisly.
  global right_target is jrti.
  global booster_separation_speed is 850.
}

static_gnc().
