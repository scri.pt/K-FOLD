// Equatorial 12: 90º (Droneship)

function static_gnc {
  global boostback_pitch is 5.
  global launch_roll_degrees is 0.
  global launch_pitchover_degrees is 12.
  global launch_pitchover_velocity is 100.
  global boostback_overshoot is 500.
  global final_velocity_goal is 80.
  global final_distance_goal is 750.
  global left_target is ocisly.
  global right_target is ocisly.
}

static_gnc().
