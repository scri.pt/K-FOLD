K-FOLD
======

> “If a thing is worth doing, it's worth doing badly.”  
> – G.K. Chesterson

Kerbal Flight Orientation and Landing Demonstration
---------------------------------------------------

K-FOLD is a fully-automated launch and booster recovery system for Kerbal
Space Program and kOS. It currently supports the Eyas 7 Heavy, a 21-engine
three-core demonstration vehicle. K-FOLD allows for arbitrary launch azimuths
and booster landing targets, and aims to accommodate a wide range of pitchover
maneuvers.

K-FOLD **is** intended as a learning tool, and aims to use concepts that can
be easily taught in a secondary education setting. Issues filed by science
educators will be given priority.

K-FOLD **is not** intended for professional aerospace engineers: if you have
any reasonable understanding of fuel-optimal GNC and/or control theory, this
software has a high probability of making you cry. You have been warned.

Video Demonstration
-------------------

[![Video Link](https://i.vimeocdn.com/video/808656107.jpg?mw=640&mh=360)](https://vimeo.com/355760805)

Software Requirements
---------------------

We recommend that you use [CKAN](https://github.com/KSP-CKAN/CKAN/releases/tag/v1.30.4)
to manage all Kerbal Space Program add-ons. The links below try to point to the last
known-good version of each required package – which may or may not be current.

  * [Kerbal Space Program v1.12.2](https://scri.pt/k-fold/link/ksp)
  * [Kerbal Operating System (kOS)](https://scri.pt/k-fold/link/kos)
  * [Trajectories](https://scri.pt/k-fold/link/trajectories)
  * [Kerbal Reusability Expansion](https://scri.pt/k-fold/link/kre)
  * [SpaceX Launch Vehicles](https://scri.pt/k-fold/link/slv)
  * [Kerbal Engineer](https://scri.pt/k-fold/link/ke) (Optional)

Earlier or later versions *may* work, but are not officially supported.
The Trajectories v2.x series currently has a known issue related to kOS
integration, and may not work properly with K-FOLD at this time.

Installing K-FOLD
-----------------

  0. Download all required software listed above.

  1. Install Kerbal Space Program. Ensure it starts properly.

  2. Create a new game in _sandbox_ mode, or locate and back up an
     existing game that was created in _sandbox_ mode. Take note of
     its name. We will refer to this name as `$GAME` in the following
     instructions.

  3. Locate your Kerbal Space Program installation. Take note of its
     fully-qualified filesystem path. We will refer to this path as
     `$KSP` in the following instructions.

  4. Extract the contents of each downloaded ZIP archive using your
     favorite decompression utility. If the archive contains a `GameData`
     folder, copy its contents to your `$KSP/GameData` folder. Otherwise,
     copy the entire contents of the archive to your `$KSP/GameData` folder.
     In any case, you should be copying a single folder and a small number
     of files.

  5. Use `git` to clone this repository. By default, this will create
     a folder named `K-FOLD`. Copy `K-FOLD` to `$KSP/Ships/Script/K-FOLD`.
     You may need to create the `Script` folder by hand.

  6. Copy the contents of `$KSP/Ships/Script/K-FOLD/craft` to
     `$KSP/saves/$GAME/Ships/VAB`.

  7. Copy the contents of `$KSP/Ships/Script/K-FOLD/saves` to
     `$KSP/saves/$GAME`.

  8. Restart Kerbal Space Program.

Running K-FOLD
--------------

  0. Start the saved game you used above. On Windows, press `Alt` and `F9`
     simultaneously. On Linux, press `Right-Shift` and `F9` simultaneously.
     On Mac OS X, press `Option` and `F9` simultaneously. Select `K-FOLD`
     from the resulting quicksave menu.

  1. Enter the Vehicle Assembly Building (VAB), open the Eyas 7 Heavy
     spacecraft, then press the green launch button.

  2. Click the kOS toolbar button. You should see three processors:
     `left` and `right` (side booster CPUs), and `core` (primary CPU).

  3. Execute `runpath("0:/K-FOLD/src/main.ks").` on each kOS processor,
     starting with the side booster CPUs. When this command is executed
     on the primary CPU, the countdown will begin.

  4. Godspeed. Upon separation, switch your view to the left booster
     using the `]` key to follow it back down to the landing zone.

  5. In some cases, the first launch may fail with a kOS error referring
     to `HasImpact`. This appears to be a one-time transient issue. If
     this happens, try the launch again. For more information, consult
     the K-FOLD issue repository.

Known Limitations
-----------------

There are a large number of known limitations and potential improvements.
Guidance and navigation control is currently a minimal stub implementation
that lacks even basic parabolic correction. No part of this system is
fuel-optimal. For more information, please see the K-FOLD issue repository.

Filing Issues
-------------

> “On the whole, I'm glad he was never a professional.
>  It might have stood in his way of becoming an amateur.”  
> – G.K. Chesterson

This project was created for fun as a learning tool. Please feel free
to file issues regarding bugs/incompatibilities, proposed improvements,
documentation, issues relevant to use in science education, or feature
requests you are willing to help work on.

Issues other than the above that are incomplete, hostile, rude, or
unconstructive in nature may be closed and/or removed without notice.

License
-------

K-FOLD is released under the GNU General Public License, version three.
If you distribute a modified version of this software, or use a modified
version of this software as part of a service or exhibition (including
video production), you are obligated to make your source code modifications
available to the public.

If you are using this software in an elementary, secondary, or post-secondary
education setting, these restrictions can and will be waived upon request.

Author
------

David Brown  
Portland, Oregon, USA

